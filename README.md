# experiment-push

This repository will commit in CI to itself  but the logic comes from a git submodule

## Workflow

On each run (scheduled or manual) the CI script will add 

- date and time
- a random number

to the repo and commit/push it.
